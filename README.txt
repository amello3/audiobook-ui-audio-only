# Audiobook User Interface

Credits to Anthony Hornoff for the skeleton version of this code.

# Author:
---------
-Austin Mello

# Contact information:
----------------------
- Email: amello3@uoregon.edu

- Phone: 530-276-1662

- Zoom: amello3@uoregon.edu

# Description:
--------------
- This code is a series of audio menus that forms an audiobook player without
  the need for a graphical user interface.
- It consists of three functions, plus the main(), that create 3 separate 
  menus: A main menu, a table of contents menu, and a pause menu.
- Each menu allows the user to utilize five buttons to perform various 
  ways to navigate the audiobook player.  The buttons inside each menu are 
  unique to that menu.
- The bookmark feature reads and writes to an outside text file that stores the
  current chapter and a timestamp of that chapter, so that the user can close 
  the program entirely, reopen it, and continue where he/she left off.

# Notes:
--------
- VOLUME WARNING: The TTS narration inside the menu can be loud if you don't 
  expect it.  I recommend turning your volume down to 50% when you run the
  program.
- The audio files that compose the audiobook are quite large, the largest
  being over 50Mb.  This being the case, I have not uploaded them to this
  repository.