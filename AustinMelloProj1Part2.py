# Initial book-reader
# Written by Austin Mello - 10-11-2020
# Derived from pygame_nerdparadise_2.py by Anthony Hornof - 9-30-2020
# Derived from PlayClipBegEnd.py by Anthony Hornof - 10-9-2020
# Derived from PlayTwoChannels.py by Anthony Hornof - 10-11-2020
# Reading/Writing to .txt files made possible by https://realpython.com/read-write-files-python/#reading-and-writing-opened-files
# Based on code found at:
# https://nerdparadise.com/programming/pygame/part6


# This is part one of project one.  It is a prototype of the whole project.
# The main menu and table of contents are featured here.

#Todo:
#   Add parts 3-12, create play and pause menus, create bookmark tool.


# Sometimes it stops responding to keys, and the keystrokes appear on the display.
#   This seems to be reliably reproduced by shifting focus from Terminal.app.
# __ Next: Advance through a sequence of numbers.
# __ How do I need to change the audio file to work correctly?
# wav files do not work: Position not implemented for music type wav.
# I am using mp3 Layer 3


import pygame
import sys

class Bookmark:
    def __init__(self, chapter, timestamp):
        self.chapter = 0
        self.timestamp = 0

    

def mainMenu():
    #Play main menu options
    pygame.mixer.music.load("mainMenu.ogg")
    pygame.mixer.music.play(start = 0)

    #playCode is passed to the playMenu, and tells it where to start playing
    bookmark = Bookmark(0,0)
    playCode = 0

    #Conditionals for different key presses
    while True:
        
        pressed = pygame.key.get_pressed()

        for event in pygame.event.get():
            
            if event.type == pygame.QUIT:
                print("Quit 1")
                return

            if event.type == pygame.KEYDOWN:
                # determine if a letter key was pressed
                # 'A' to start from beginning
                if event.key == pygame.K_a:
                    print("Start")
                    playMenu(bookmark)
                    return

                # 'S' for table of contents
                if event.key == pygame.K_s:
                    print("Table of Contents")
                    tocMenu(i = 0)
                    return

                # 'D' to load from bookmark (function to be added)
                if event.key == pygame.K_d:
                    print("Loading from Bookmark")
                    l = [0,0]
                    i=0
                    #Reads from Bookmark.txt the chapter and timestamp
                    #Stores in the bookmark class
                    with open('Bookmark.txt', 'r') as marker:
                        for line in marker:
                            l[i] = int(line)
                            i += 1

                    bookmark.chapter = l[0]
                    bookmark.timestamp = l[1]

                    print("Chapter: ", bookmark.chapter)
                    print("Timestamp: ", bookmark.timestamp)
                    
                    playMenu(bookmark)
                    return

                # 'F' to Exit
                if event.key == pygame.K_f:
                    print("Exiting...")
                    pygame.quit()
                    return

                if event.key == pygame.K_SPACE:
                    mainMenu()
                    return
                    

def playMenu(x):
    #Play menu
    #Adding the menu later, for now, it just plays the requested chapter

    #list of available chapters
    bookmark = x
    chapters = ["001-DesignEveryday_Preface_2002.ogg", "002-DesignEveryday_Preface_1988.ogg",
                "003-DesignEveryday_Chapter_1.ogg", "004-DesignEveryday_Chapter_2.ogg"]
    newStartTime = 0
    paused = False
    
    #Plays the Play Menu options.
    print("Play Menu")
    pygame.mixer.music.load("playMenu.ogg")
    pygame.mixer.music.play(start = 0/ 1000)

    while pygame.mixer.music.get_busy():
        pass

    #Plays requested chapter based on the 'play code'
    pygame.mixer.music.load(chapters[bookmark.chapter])

    #queue the subsequent chapters
    i = bookmark.chapter + 1
    while i < len(chapters):
        i += 1
        pygame.mixer.music.queue(chapters[i - 1])

        
    pygame.mixer.music.play(start = bookmark.timestamp / 1000)
    
    
    while True:

        pygame.mixer.music

        pressed = pygame.key.get_pressed()

        for event in pygame.event.get():
            
            if event.type == pygame.QUIT:
                print("Quit 1")
                return

            if event.type == pygame.KEYDOWN:
                # determine if a letter key was pressed

                # 'A' to skip 15 seconds
                if event.key == pygame.K_a:
                    print("Skipping 15 seconds")
                    newStartTime += 15
                    pygame.mixer.music.play(start = newStartTime)

                # 'S' to reqind 15 seconds
                if event.key == pygame.K_s:
                    print("Rewinding 15 seconds")
                    newStartTime -= 15
                    pygame.mixer.music.play(start = newStartTime)

                # 'D' to pause/play
                if event.key == pygame.K_d:
                    #Check if player is paused or playing.  Toggle accordingly.
                    if paused == False:
                        print("Player paused")
                        pygame.mixer.music.pause()
                        paused = True

                    else:
                        if event.key == pygame.K_d:
                            print("Player unpaused")
                            pygame.mixer.music.unpause()
                            paused = False

                # 'F' to Exit
                if event.key == pygame.K_f:
                    print("Exiting...")
                    pygame.quit()
                    return

                # 'SPACE' to create a bookmark
                if event.key == pygame.K_SPACE:
                    print("creating a bookmark")
                    #Create a timestamp
                    bookmark.timestamp = pygame.mixer.music.get_pos()

                    with open('Bookmark.txt', 'w') as writer:
                        writer.write(str(bookmark.chapter))
                        writer.write('\n')
                        writer.write(str(bookmark.timestamp))
                        
                        

def tocMenu(i):
    #Table of Contents menu
    print("Table of Contents Menu")

    #List of available chapters to iterate through
    tocList = ["Preface2002.ogg", "Preface1998.mp3", "Chapter1.mp3", "Chapter2.mp3"]

    playCode = 0    
    
    #Conditional to wrap through the tocList
    if i > 3:
        i = 0
    if i < 0:
        i = 3  

    pygame.mixer.music.load(tocList[i])
    print(tocList[i])
    pygame.mixer.music.play(start = 0 / 1000)

    while pygame.mixer.music.get_busy():
        pass
    
    pygame.mixer.music.load("tableOfContents.ogg")
    pygame.mixer.music.play(start = 0 / 1000)
    
    
    #Conditionals for different key presses
    while True:
        
        pressed = pygame.key.get_pressed()

        for event in pygame.event.get():
            
            if event.type == pygame.QUIT:
                print("Quit 1")
                return

            if event.type == pygame.KEYDOWN:
                # determine if a letter key was pressed

                # 'A' to play selected chapter
                if event.key == pygame.K_a:
                    print("Starting")
                    bookmark = Bookmark(0, 0)
                    bookmark.chapter = i
                    playMenu(bookmark)
                    return

                # 'S' for the next chapter
                if event.key == pygame.K_s:
                    print("Next chapter")
                    i = i + 1
                    tocMenu(i)
                    return

                # 'D' for the previous chapter
                if event.key == pygame.K_d:
                    print("Previous chapter")
                    i = i - 1
                    tocMenu(i)
                    return

                # 'F' to main menu
                if event.key == pygame.K_f:
                    print("Returning to main menu")
                    mainMenu()
                    return

                # 'SPACE' to repeat the options
                if event.key == pygame.K_SPACE:
                    tocMenu(i)
                    return
    

def main():
    #Initiate pygame, then calls the mainMenu function.

    pygame.init()
    screen = pygame.display.set_mode((300, 100))
    pygame.mixer.init()

    clock = pygame.time.Clock()
    new_start_time = 0  # Used to advance the play()

    mainMenu()

main()
